terraform {
  backend "azurerm" {
    storage_account_name = ""
    container_name = ""
    key = ""
    subscription_id = ""
    tenant_id = ""
  }
}